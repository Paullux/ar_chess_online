extends Spatial

var Pieces3D = preload("res://_Objet_3D/Pièces3D.tscn")
var Pos = Vector3(0,0,0)
var matrix=[]
var first = true

#var arvr_interface = ARVRServer.find_interface("Native mobile")

func _ready():
	#if arvr_interface and arvr_interface.initialize():
	#	get_viewport().arvr = false
	for x in range(0,9):
		matrix.append([])
		matrix[x].resize(9)
		for z in range(0,9):
			matrix[x][z] = 0
	matrix[1][1] = 6
	matrix[1][2] = 1
	matrix[1][3] = 2
	matrix[1][4] = 4
	matrix[1][5] = 5
	matrix[1][6] = 2
	matrix[1][7] = 1
	matrix[1][8] = 6
	matrix[8][1] = 12
	matrix[8][2] = 7
	matrix[8][3] = 8
	matrix[8][4] = 10
	matrix[8][5] = 11
	matrix[8][6] = 8
	matrix[8][7] = 7
	matrix[8][8] = 12
	for value in range (1,9):
		matrix[2][value] = 3
		matrix[7][value] = 9
	set_process(true)
	move_pieces()

func _process(delta):
	pass

func move_pieces():
	for xposition in range (1, 9):
		Pos.x = -1 * ((5 - xposition) * 0.125 - 0.125 / 2)
		for zposition in range (1, 9):
			Pos.z = (5 - zposition) * 0.125 - 0.125 / 2
			Pos.y = 0
			var locationNode = Pieces3D.instance()
			locationNode.translation = Pos
			get_node('/root/Spatial/Echiquier').add_child(locationNode)
			for myNode in range (0,12):
				locationNode.get_child(myNode).hide()
				if matrix[zposition][xposition] == 0:
					locationNode.hide()
				else:
					locationNode.show()
				if matrix[zposition][xposition] == myNode + 1:
					locationNode.get_child(myNode).show()